FROM ubuntu:18.04

RUN apt-get update -y && \
    apt-get install -y cmake g++ mosquitto-clients mosquitto

RUN apt-get install -y libmosquitto-dev

COPY ./mosquitto.conf /etc/mosquitto/mosquitto.conf

RUN mkdir /opt/mosquitto_c
WORKDIR /opt/mosquitto_c

VOLUME /opt/mosquitto_c

ENTRYPOINT /usr/sbin/mosquitto -d -c /etc/mosquitto/mosquitto.conf && /bin/bash