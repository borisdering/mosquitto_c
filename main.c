#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "mosquitto.h"

static bool running = true;

/**
 * On message callback for when a message was received.
 * @param mosquitto
 * @param user_data
 * @param message
 * @return
 */
int on_message(struct mosquitto *client, void *user_data, const struct mosquitto_message *message)
{
    printf("%s %s (%d)\n", message->topic, (const char *)message->payload, message->payloadlen);
    return 0;
}

/**
 *
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char** argv)
{

    // initialize the mosquitto library
    mosquitto_lib_init();

    // try to figure out version and display it
    // to the console
    int major = 0, minor = 0, revision = 0;
    mosquitto_lib_version(&major, &minor, &revision);
    printf("%d.%d.%d\n", major, minor, revision);

    char* last_will_topic = "disconnected";
    char* last_will_payload = "";

    // create client id depending on the process id
    char client_id[24];
    memset(client_id, 0, 24);
    snprintf(client_id, 23, "client_id=%d", getpid());

    // create mosquitto client instance
    struct mosquitto *client = mosquitto_new(client_id, true, NULL);

    // make sure that we have a valid client
    if (client) {

        // set message callback
        mosquitto_message_callback_set(client, on_message);

        // define last will
        mosquitto_will_set(client, last_will_topic, (int) strlen(last_will_payload), last_will_payload, 1, true);

        // connect to broker via host and prot
        int client_state = mosquitto_connect(client, "127.0.0.1", 1883, true);

        // make sure that we are connected successfully to the broker
        if (client_state != MOSQ_ERR_SUCCESS) {
            printf("[ERROR]: unable to connect to broker due to error");
            return client_state;
        }

        // subscribe to topics of interest
        mosquitto_subscribe(client, NULL, "#", 0);

        // while process is running loop forever
        while(running) {
            if (client_state == MOSQ_ERR_SUCCESS) {
                client_state = mosquitto_loop(client, -1, 0);
            } else {
                mosquitto_reconnect(client);
            }
        }

        // clean up everything
        mosquitto_destroy(client);
        mosquitto_lib_cleanup();
    }

    return 0;
}
