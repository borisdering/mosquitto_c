# Simple MQTT (Mosquitto) example written in C
This is a simple MQTT example written in C. 

## Requirements
* gcc
* cmake  
* make 
* libmosquitto

You also need a MQTT broker running locally. You could use / install Mosquitto via docker, brew or any other package manager. 

## Build and run 
To build and run this project you need to compile it and run the output of this project.
```
$ mkdir build
$ cd build
$ cmake ..
$ chmod +x mosquitto_c
$ ./mosquitto_c
```